﻿using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Techniqly.Unilog.Framework.Json.Converters;

namespace Techniqly.Unilog.Framework.Json.Factories
{
    public sealed class JsonSerializerSettingsFactory
    {
        public static JsonSerializerSettings DefaultSettings => new JsonSerializerSettings
        {
            ContractResolver = new CamelCasePropertyNamesContractResolver(),
            Formatting = Formatting.Indented,
            Converters = new List<JsonConverter>()
        };

        public void SetHttpRequestMessageConverter(JsonSerializerSettings serializerSettings, bool shouldStringifyContent)
        {
            serializerSettings.Converters.Add(new HttpRequestMessageConverter(shouldStringifyContent));
        }

        public void SetHttpResponseMessageConverter(JsonSerializerSettings serializerSettings, bool shouldStringifyContent)
        {
            serializerSettings.Converters.Add(new HttpResponseMessageConverter(shouldStringifyContent));
        }
    }
}