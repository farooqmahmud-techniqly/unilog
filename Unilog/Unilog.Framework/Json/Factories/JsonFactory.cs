﻿using System.Net.Http;
using Newtonsoft.Json;

namespace Techniqly.Unilog.Framework.Json.Factories
{
    public sealed class JsonFactory
    {
        private readonly JsonSerializerSettingsFactory _factory;
        private readonly JsonSerializerSettings _serializerSettings;

        public JsonFactory(JsonSerializerSettingsFactory factory)
        {
            _factory = factory;
            _serializerSettings = JsonSerializerSettingsFactory.DefaultSettings;
        }

        public string CreateJson(HttpRequestMessage request, bool stringifyContent)
        {
            _factory.SetHttpRequestMessageConverter(_serializerSettings, stringifyContent);
            return JsonConvert.SerializeObject(request, _serializerSettings);
        }

        public string CreateJson(HttpResponseMessage response, bool stringifyContent)
        {
            _factory.SetHttpResponseMessageConverter(_serializerSettings, stringifyContent);
            return JsonConvert.SerializeObject(response, _serializerSettings);
        }
    }
}