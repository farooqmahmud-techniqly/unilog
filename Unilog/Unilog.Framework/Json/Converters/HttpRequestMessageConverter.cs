﻿using System;
using System.Linq;
using System.Net.Http;
using System.Text;
using Newtonsoft.Json;

namespace Techniqly.Unilog.Framework.Json.Converters
{
    internal sealed class HttpRequestMessageConverter : JsonConverter
    {
        private readonly bool _writeContent;
        
        public HttpRequestMessageConverter(bool writeContent)
        {
            _writeContent = writeContent;
        }

        public override bool CanRead { get; } = false;

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var request = (HttpRequestMessage) value;

            writer.WriteStartObject();

            WriteRequestUri(writer, request);
            WriteMethod(writer, request);

            if (_writeContent)
            {
                WriteContent(writer, request);
            }

            WriteHeaders(writer, request);
            WriteVersion(writer, request);

            writer.WriteEndObject();
        }

        private void WriteVersion(JsonWriter writer, HttpRequestMessage request)
        {
            writer.WritePropertyName("version");
            writer.WriteValue(request.Version.ToString());
        }

        private void WriteHeaders(JsonWriter writer, HttpRequestMessage request)
        {
            writer.WritePropertyName("headers");
            writer.WriteStartObject();

            var headers = request.Headers;

            foreach (var header in headers)
            {
                writer.WritePropertyName(header.Key);
                writer.WriteValue(string.Join(",", header.Value.ToArray()));
            }

            writer.WriteEndObject();
        }

        private static void WriteContent(JsonWriter writer, HttpRequestMessage request)
        {
            var contentBytes = request.Content.ReadAsByteArrayAsync().Result;
            var content = Encoding.UTF8.GetString(contentBytes);

            writer.WritePropertyName("content");
            writer.WriteRawValue(content);
        }

        private static void WriteMethod(JsonWriter writer, HttpRequestMessage request)
        {
            writer.WritePropertyName("method");
            writer.WriteValue(request.Method.ToString());
        }

        private static void WriteRequestUri(JsonWriter writer, HttpRequestMessage request)
        {
            writer.WritePropertyName("uri");
            writer.WriteValue(request.RequestUri);
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(HttpRequestMessage);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue,
            JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }
}