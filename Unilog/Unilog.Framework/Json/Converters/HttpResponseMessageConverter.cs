﻿using System;
using System.Linq;
using System.Net.Http;
using System.Text;
using Newtonsoft.Json;

namespace Techniqly.Unilog.Framework.Json.Converters
{
    internal sealed class HttpResponseMessageConverter : JsonConverter
    {
        private readonly bool _writeContent;

        public HttpResponseMessageConverter(bool writeContent)
        {
            _writeContent = writeContent;
        }

        public override bool CanRead { get; } = false;

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var response = (HttpResponseMessage)value;

            writer.WriteStartObject();
            WriteStatusCode(writer, response);
            WriteReasonPhrase(writer, response);

            if (_writeContent)
            {
                WriteContent(writer, response);
            }

            WriteHeaders(writer, response);
            WriteVersion(writer, response);

            writer.WriteEndObject();
        }

        private void WriteReasonPhrase(JsonWriter writer, HttpResponseMessage response)
        {
            writer.WritePropertyName("reasonPhrase");
            writer.WriteValue(response.ReasonPhrase);
        }

        private void WriteStatusCode(JsonWriter writer, HttpResponseMessage response)
        {
            writer.WritePropertyName("statusCode");
            writer.WriteValue(response.StatusCode);
        }

        private void WriteVersion(JsonWriter writer, HttpResponseMessage response)
        {
            writer.WritePropertyName("version");
            writer.WriteValue(response.Version.ToString());
        }

        private void WriteHeaders(JsonWriter writer, HttpResponseMessage response)
        {
            writer.WritePropertyName("headers");
            writer.WriteStartObject();

            var headers = response.Headers;

            foreach (var header in headers)
            {
                writer.WritePropertyName(header.Key);
                writer.WriteValue(string.Join(",", header.Value.ToArray()));
            }

            writer.WriteEndObject();
        }

        private static void WriteContent(JsonWriter writer, HttpResponseMessage response)
        {
            var contentBytes = response.Content.ReadAsByteArrayAsync().Result;
            var content = Encoding.UTF8.GetString(contentBytes);

            writer.WritePropertyName("content");
            writer.WriteRawValue(content);
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(HttpResponseMessage);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue,
            JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }
}