﻿using System.Collections.Generic;
using System.Net.Http;

namespace Techniqly.Unilog.Framework.LogSources
{
    public interface IWebApiLogSource
    {
        void LogApiRequest(HttpRequestMessage request, IList<KeyValuePair<string, object>> additionalProperties, bool logContent);
        void LogApiResponse(HttpResponseMessage response, IList<KeyValuePair<string, object>> additionalProperties, bool logContent);
    }
}