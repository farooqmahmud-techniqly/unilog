﻿using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Techniqly.Unilog.Framework")]
[assembly: AssemblyDescription("An easy to use semantic logging library")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Techniqly")]
[assembly: AssemblyProduct("Techniqly.Unilog.Framework")]
[assembly: AssemblyCopyright("Copyright © Techniqly 2016")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]
[assembly: Guid("53DFA56D-2AAC-478C-9283-B50351ACF8FC")]
[assembly: AssemblyVersion("1.1.*")]
[assembly: AssemblyFileVersion("1.1.*")]
[assembly: CLSCompliant(true)]
[assembly: InternalsVisibleTo("Techniqly.Unilog.Framework.Tests")]