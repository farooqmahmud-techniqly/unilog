﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Formatting;
using System.Reflection;
using System.Web.Http;
using Autofac;
using Autofac.Integration.WebApi;
using AutofacSerilogIntegration;
using Newtonsoft.Json.Serialization;
using Serilog;
using Techniqly.Unilog.Framework.LogSources;
using Techniqly.Unilog.Serilog;
using TestWebApi.MessageHandlers;

namespace TestWebApi
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            ConfigureSerializer(config);

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            ConfigureAutofac(config);
        }

        private static void ConfigureAutofac(HttpConfiguration config)
        {
            var builder = new ContainerBuilder();
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            builder.RegisterWebApiFilterProvider(config);

            builder.RegisterType<WebApiLogSource>().As<IWebApiLogSource>();

            Log.Logger = new LoggerConfiguration().Enrich.FromLogContext().ReadFrom.AppSettings().CreateLogger();
            builder.RegisterLogger();

            var container = builder.Build();
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);

            config.MessageHandlers.Add(new MessageLoggingHandler(container.Resolve<IWebApiLogSource>()));

        }

        private static void ConfigureSerializer(HttpConfiguration config)
        {
            config.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
        }
    }
}
