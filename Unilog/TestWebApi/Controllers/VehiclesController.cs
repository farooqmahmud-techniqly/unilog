﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace TestWebApi.Controllers
{
    [RoutePrefix("api/vehicles")]
    public class VehiclesController : ApiController
    {
        [Route("{id}")]
        [HttpGet]
        public IHttpActionResult Get(string id)
        {
            return Ok(new {Make = "Ford", Model = "Edge", ModelYear = 2016, Id = id});
        }
    }
}
