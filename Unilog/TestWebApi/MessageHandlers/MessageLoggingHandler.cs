﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Techniqly.Unilog.Framework.LogSources;

namespace TestWebApi.MessageHandlers
{
    public class MessageLoggingHandler : DelegatingHandler
    {
        private readonly IWebApiLogSource _webApiLogSource;

        public MessageLoggingHandler(IWebApiLogSource webApiLogSource)
        {
            _webApiLogSource = webApiLogSource;
        }

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            _webApiLogSource.LogApiRequest(request, new List<KeyValuePair<string, object>>
            {
                new KeyValuePair<string, object>("Message", "Incoming request.")
            },
            true);

            var response = await base.SendAsync(request, cancellationToken);

            _webApiLogSource.LogApiResponse(response, new List<KeyValuePair<string, object>>
            {
                new KeyValuePair<string, object>("Message", "Response")
            }, 
            true);

            return response;
        }
    }
}