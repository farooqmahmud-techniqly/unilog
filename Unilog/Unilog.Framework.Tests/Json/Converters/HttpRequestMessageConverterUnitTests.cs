﻿using System;
using System.IO;
using FluentAssertions;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using NUnit.Framework;
using Techniqly.Unilog.Framework.Json.Converters;

namespace Techniqly.Unilog.Framework.Tests.Json.Converters
{
    [TestFixture]
    public class HttpRequestMessageConverterUnitTests
    {
        private HttpRequestMessageConverter _converter;

        [SetUp]
        public void SetUp()
        {
            _converter = new HttpRequestMessageConverter(false);
        }

        [Test]
        public void CanRead_ReturnsFalse()
        {
            _converter.CanRead.Should().BeFalse();
        }

        [Test]
        public void ReadJson_ThrowsException()
        {
           Assert.Throws<NotImplementedException>(
               () => _converter.ReadJson(
                   new BsonReader(new MemoryStream()), 
                   typeof(object), 
                   null, 
                   new JsonSerializer()));
        }
    }
}
