﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using FluentAssertions;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using Techniqly.Unilog.Framework.Json.Factories;

namespace Techniqly.Unilog.Framework.Tests.Json.Factories
{
    [TestFixture]
    public class JsonFactoryUnitTests
    {
        [SetUp]
        public void SetUp()
        {
            _defaultRequest = CreateDefaultRequest();
            _defaultResponse = CreateDefaultResponse();
            _jsonFactory = GetDefaultJsonFactory();
            _jsonSerializerSettings = GetDefaultSerializerSettings();
        }

        private JsonFactory GetDefaultJsonFactory()
        {
            return new JsonFactory(new JsonSerializerSettingsFactory());
        }

        private JsonSerializerSettings GetDefaultSerializerSettings()
        {
            return JsonSerializerSettingsFactory.DefaultSettings;
        }

        private HttpRequestMessage CreateDefaultRequest()
        {
            var request = new HttpRequestMessage(HttpMethod.Post, Uri);

            request.Headers.Authorization = new AuthenticationHeaderValue("Basic", "ABC123");
            request.Headers.Add("h1", "v1");
            request.Headers.Add("h2", "v2");
            request.Properties.Add("p1", "v1");
            request.Properties.Add("p2", "v2");

            return request;
        }

        private HttpResponseMessage CreateDefaultResponse()
        {
            var response = new HttpResponseMessage(HttpStatusCode.Created);

            response.Headers.Location = new Uri(Uri);
            response.ReasonPhrase = "Created";

            return response;
        }

        private HttpRequestMessage _defaultRequest;
        private HttpResponseMessage _defaultResponse;
        private static readonly string Uri = "http://wwww.blah.com/api";

        private JsonFactory _jsonFactory;
        private JsonSerializerSettings _jsonSerializerSettings;

        private void AddDefaultContentToRequest(HttpRequestMessage request)
        {
            request.Content = GetDefaultContent();
        }

        private dynamic GetDefaultContentAsDynamicObject()
        {
            return new {Make = "Ford", Model = "Edge", ModelYear = 2016};
        }

        private HttpContent GetDefaultContent()
        {
            var obj = GetDefaultContentAsDynamicObject();

            var content = new StringContent(JsonConvert.SerializeObject(obj, _jsonSerializerSettings));
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            return content;
        }

        private void AddDefaultContentToResponse(HttpResponseMessage response)
        {
            response.Content = GetDefaultContent();
        }

        private static void VerifyUri(HttpRequestMessage originalRequest, dynamic deserializedRequest)
        {
            ((string) deserializedRequest.uri).Should().Be(originalRequest.RequestUri.ToString());
        }

        private static void VerifyVersion(Version expectedVersion, string actualVersion)
        {
            actualVersion.Should().Be(expectedVersion.ToString());
        }

        private static void VerifyMethod(HttpRequestMessage originalRequest, dynamic deserializedRequest)
        {
            ((string) deserializedRequest.method).ToUpper().Should().Be(originalRequest.Method.ToString());
        }

        private static void VerifyHeaders(HttpHeaders expectedHeaders, dynamic deserializedRequest)
        {
            var headers = ((JObject) deserializedRequest.headers).Properties().ToArray();

            for (var i = 0; i < headers.Length; i++)
            {
                headers[i].Name.Should().Be(expectedHeaders.ElementAt(i).Key);

                var value = (string) headers[i].Value;
                value.Should().Be(string.Join(",", expectedHeaders.ElementAt(i).Value));
            }
        }

        private void VerifyReasonPhrase(HttpResponseMessage originalResponse, dynamic deserializedResponse)
        {
            ((string) deserializedResponse.reasonPhrase).Should().Be(originalResponse.ReasonPhrase);
        }

        private void VerifyStatusCode(HttpResponseMessage originalResponse, dynamic deserializedResponse)
        {
            ((int) deserializedResponse.statusCode).Should().Be((int) originalResponse.StatusCode);
        }

        [Test]
        public void CreateJson_ConvertsHttpRequestMessageToJson()
        {
            var stringifyContent = true;

            AddDefaultContentToRequest(_defaultRequest);

            var json = _jsonFactory.CreateJson(_defaultRequest, stringifyContent);
            json.Should().NotBeNullOrWhiteSpace();

            var deserializedRequest = JsonConvert.DeserializeObject<dynamic>(json);

            VerifyUri(_defaultRequest, deserializedRequest);
            VerifyVersion(_defaultRequest.Version, (string) deserializedRequest.version);
            VerifyMethod(_defaultRequest, deserializedRequest);
            VerifyHeaders(_defaultRequest.Headers, deserializedRequest);

            var content = deserializedRequest.content;
            var obj = GetDefaultContentAsDynamicObject();

            ((string) content.make).Should().Be(obj.Make);
            ((string) content.model).Should().Be(obj.Model);
            ((int) content.modelYear).Should().Be(obj.ModelYear);
        }

        [Test]
        public void CreateJson_ConvertsHttpRequestMessageToJson_ButNotTheContent()
        {
            var stringifyContent = false;

            AddDefaultContentToRequest(_defaultRequest);

            var json = _jsonFactory.CreateJson(_defaultRequest, stringifyContent);
            json.Should().NotBeNullOrWhiteSpace();

            var deserializedRequest = JsonConvert.DeserializeObject<dynamic>(json);

            VerifyUri(_defaultRequest, deserializedRequest);
            VerifyVersion(_defaultRequest.Version, (string) deserializedRequest.version);
            VerifyMethod(_defaultRequest, deserializedRequest);
            VerifyHeaders(_defaultRequest.Headers, deserializedRequest);

            var content = deserializedRequest.content;
            Assert.IsNull(content);
        }

        [Test]
        public void CreateJson_ConvertsHttpResponseMessageToJson()
        {
            var stringifyContent = true;
            AddDefaultContentToResponse(_defaultResponse);

            var json = _jsonFactory.CreateJson(_defaultResponse, stringifyContent);
            json.Should().NotBeNullOrWhiteSpace();

            var deserializedResponse = JsonConvert.DeserializeObject<dynamic>(json);

            VerifyVersion(_defaultResponse.Version, (string) deserializedResponse.version);
            VerifyHeaders(_defaultResponse.Headers, deserializedResponse);
            VerifyReasonPhrase(_defaultResponse, deserializedResponse);
            VerifyStatusCode(_defaultResponse, deserializedResponse);

            var content = deserializedResponse.content;
            var obj = GetDefaultContentAsDynamicObject();

            ((string) content.make).Should().Be(obj.Make);
            ((string) content.model).Should().Be(obj.Model);
            ((int) content.modelYear).Should().Be(obj.ModelYear);
        }

        [Test]
        public void CreateJson_ConvertsHttpResponseMessageToJson_ButNotTheContent()
        {
            var stringifyContent = false;
            AddDefaultContentToResponse(_defaultResponse);

            var json = _jsonFactory.CreateJson(_defaultResponse, stringifyContent);
            json.Should().NotBeNullOrWhiteSpace();

            var deserializedResponse = JsonConvert.DeserializeObject<dynamic>(json);

            VerifyVersion(_defaultResponse.Version, (string) deserializedResponse.version);
            VerifyHeaders(_defaultResponse.Headers, deserializedResponse);
            VerifyReasonPhrase(_defaultResponse, deserializedResponse);
            VerifyStatusCode(_defaultResponse, deserializedResponse);

            var content = deserializedResponse.content;
            Assert.IsNull(content);
        }
    }
}