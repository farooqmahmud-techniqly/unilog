﻿using System.Collections.Generic;
using System.Net.Http;
using Serilog;
using Serilog.Context;
using Serilog.Core;
using Serilog.Core.Enrichers;
using Techniqly.Unilog.Framework.Json.Factories;
using Techniqly.Unilog.Framework.LogSources;

namespace Techniqly.Unilog.Serilog
{
    public sealed class WebApiLogSource : IWebApiLogSource
    {
        private readonly ILogger _logger;

        public WebApiLogSource(ILogger logger)
        {
            _logger = logger;
        }

        public void LogApiRequest(HttpRequestMessage request, IList<KeyValuePair<string, object>> additionalProperties,
            bool logContent)
        {
            if (logContent)
            {
                LogRequestVerbose(request, additionalProperties);
            }
            else
            {
                LogRequestInformational(request, additionalProperties);
            }
        }

        public void LogApiResponse(HttpResponseMessage response,
            IList<KeyValuePair<string, object>> additionalProperties, bool logContent)
        {
            if (logContent)
            {
                LogResponseVerbose(response, additionalProperties);
            }
            else
            {
                LogResponseInformational(response, additionalProperties);
            }
        }

        private void LogRequestInformational(HttpRequestMessage request,
            IList<KeyValuePair<string, object>> additionalProperties)
        {
            var propertyEnrichers = new List<ILogEventEnricher>();

            foreach (var additionalProperty in additionalProperties)
            {
                propertyEnrichers.Add(new PropertyEnricher(additionalProperty.Key, additionalProperty.Value));
            }

            using (LogContext.PushProperties(propertyEnrichers.ToArray()))
            {
                var jsonFactory = new JsonFactory(new JsonSerializerSettingsFactory());
                var json = jsonFactory.CreateJson(request, false);
                _logger.Information(json);
            }
        }

        private void LogRequestVerbose(HttpRequestMessage request,
            IList<KeyValuePair<string, object>> additionalProperties)
        {
            var propertyEnrichers = new List<ILogEventEnricher>();

            foreach (var additionalProperty in additionalProperties)
            {
                propertyEnrichers.Add(new PropertyEnricher(additionalProperty.Key, additionalProperty.Value));
            }

            using (LogContext.PushProperties(propertyEnrichers.ToArray()))
            {
                var jsonFactory = new JsonFactory(new JsonSerializerSettingsFactory());
                var json = jsonFactory.CreateJson(request, true);
                _logger.Verbose(json);
            }
        }

        private void LogResponseInformational(HttpResponseMessage response,
            IList<KeyValuePair<string, object>> additionalProperties)
        {
            var propertyEnrichers = new List<ILogEventEnricher>();

            foreach (var additionalProperty in additionalProperties)
            {
                propertyEnrichers.Add(new PropertyEnricher(additionalProperty.Key, additionalProperty.Value));
            }

            using (LogContext.PushProperties(propertyEnrichers.ToArray()))
            {
                var jsonFactory = new JsonFactory(new JsonSerializerSettingsFactory());
                var json = jsonFactory.CreateJson(response, false);
                _logger.Information(json);
            }
        }

        private void LogResponseVerbose(HttpResponseMessage response,
            IList<KeyValuePair<string, object>> additionalProperties)
        {
            var propertyEnrichers = new List<ILogEventEnricher>();

            foreach (var additionalProperty in additionalProperties)
            {
                propertyEnrichers.Add(new PropertyEnricher(additionalProperty.Key, additionalProperty.Value));
            }

            using (LogContext.PushProperties(propertyEnrichers.ToArray()))
            {
                var jsonFactory = new JsonFactory(new JsonSerializerSettingsFactory());
                var json = jsonFactory.CreateJson(response, false);
                _logger.Verbose(json);
            }
        }
    }
}